swh.core[http] >= 0.3  # [http] is required by swh.core.pytest_plugin
swh.lister >= 2.9.0
swh.loader.core >= 5.2.0
swh.storage >= 1.8.0
